package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public String computerRolls(){
        Random rand = new Random();
        int randomIndex = rand.nextInt(rpsChoices.size());
        String computerRoll = rpsChoices.get(randomIndex);
        return computerRoll;
    }

    public String whoWins(String roll1, String roll2){
        String roundWinner = new String(); 
        if (roll1.equals(roll2)){
            roundWinner = "None";
        }
        else if (roll1.equals("paper")){
            if (roll2.equals("rock")){
                roundWinner = "Human";
                humanScore++;
            }
            else {
                roundWinner = "Computer";
                computerScore++;
            }
        }
        else if (roll1.equals("rock")){
            if (roll2.equals("scissors")){
                roundWinner = "Human";
                humanScore++;
            }
            else {
                roundWinner = "Computer";
                computerScore++;
            }
        }
        else if (roll1.equals("scissors")){
            if (roll2.equals("paper")){
                roundWinner = "Human";
                humanScore++;
            }
            else {
                roundWinner = "Computer";
                computerScore++;
            }
        }
        roundCounter++;
        return roundWinner;
    }
    
    public String choiceValidation(){
        while (true){
            
            String tryWord = readInput("Your choice (Rock/Paper/Scissors)?");
            // System.out.printf("%s\n" , tryWord);
            if (rpsChoices.contains(tryWord)){
                String roll1 = tryWord;
                return roll1;}
            System.out.printf("I do not understand %s. Could you try again?\n", tryWord);
            }
        }

    public void run() {
        while (true){
            System.out.printf("Let's play round %d\n", roundCounter);
            String roll1 = choiceValidation();
            String roll2 = computerRolls();
            String roundWinner = whoWins(roll1, roll2);
            if (roundWinner.equals("None")){
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", roll1, roll2);
            }
            else{
                System.out.printf("Human chose %s, computer chose %s. %s wins!\n", roll1, roll2, roundWinner);
            }
            System.out.printf("Score: human %d, computer %d \n", humanScore, computerScore);
            String replay = readInput("Do you wish to continue playing? (y/n)?");
            // System.out.printf("%s \n",replay);
            if (replay.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        return userInput;
    }
}
